1. git pull origin master (si ya tienen clonado el proyecto, este comando se debe hacer estando en la carpeta del proyecto)
usar cd para moverse entre carpetas
Despues de actualizar el codigo:
Seguir los siguientes pasos, si al usar el comando 6 se ejecuta el servidor quiere decir que todo quedo bien:
2. cd bank_be
3. python -m venv env (esto solo se realiza una vez)
4. env\Scripts\activate (cada vez que entre al proyecto deben activar el entorno virtual)
5. pip install -r requirements.txt (con este comando se instalan los paquetes, se debe usar cada vez que se agreguen paquetes)
6. python manage.py runserver