from django.urls import path
from authApp.views.doctorView import DoctorView
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from authApp import views
urlpatterns = [
    path('login/', TokenObtainPairView.as_view()
         ),  # TokenObtainPairView.as_view() '''views.homepage'''
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('medicalAppointment/', views.MedicalAppointmentView.as_view()),
    path('medicalAppointment/<int:pk>/', views.MedicalAppointmentView.as_view()),
    path('alluser/', views.UserView.as_view()),
    path('doctor/', views.DoctorView.as_view()),
]
