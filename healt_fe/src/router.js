//import { createRouter, createWebHistory, routerKey } from 'vue-router'
import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';
import LogIn from './components/Login.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import MyAppointments from './components/MyAppointments.vue'
import medicalAppointment from './components/medicalAppointment.vue'
const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: "signUp",
    component: SignUp
  },
  {
    path: '/user/home',
    name: "home",
    component: Home
  },
  {
    path: '/user/account',
    name: "account",
    component: Account
  },
  {
    path: '/user/appointments',
    name: "MyAppointments",
    component: MyAppointments
  },
  {
    path: '/user/medicalAppointment',
    name: "medicalAppointment",
    component: medicalAppointment
  }
];
const router =createRouter({
    history:createWebHistory(),
    routes,
})

export default router;