from rest_framework import serializers
from authApp.models.doctor import Doctor


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = ['id', 'name', 'specialty']

    def create(self, validated_data):
        doctorInstance = Doctor.objects.create(**validated_data)
        return doctorInstance

    def to_representation(self, obj):
        medicalDoctor = Doctor.objects.get(id=obj.id)
        return {
            'id': medicalDoctor.id,
            'name': medicalDoctor.name,
            'specialty': medicalDoctor.specialty,
        }
