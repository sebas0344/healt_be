from rest_framework import serializers
from authApp.models.medicalAppointment import MedicalAppointment


class appointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalAppointment
        fields = ['id', 'userId', 'date', 'docId']

    def create(self, validated_data):
        appointmentInstance = MedicalAppointment.objects.create(
            **validated_data)
        return appointmentInstance

    def to_representation(self, obj):
        medicalAppointment = MedicalAppointment.objects.get(id=obj.id)
        return {
            'id': medicalAppointment.id,
            'userId': medicalAppointment.userId,
            'date': medicalAppointment.date,
            'docId': medicalAppointment.docId,
        }
