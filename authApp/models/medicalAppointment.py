from django.db import models


class MedicalAppointment(models.Model):
    id = models.AutoField(primary_key=True)
    userId = models.IntegerField()
    date = models.IntegerField()
    docId = models.IntegerField()
