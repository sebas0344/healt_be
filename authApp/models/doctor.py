from django.db import models


class Doctor(models.Model):
    Dermatologists = 'Dermatologists'
    General = 'General'
    Oncologists = 'Oncologists'

    SPECIALTY_DOCTORS = [(Dermatologists, 'Dermatologists'),
                         (General, 'General'), (Oncologists, 'Oncologists')]
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    specialty = models.CharField(
        max_length=255, choices=SPECIALTY_DOCTORS)
