from rest_framework import status, views
from rest_framework.response import Response
from authApp.models import Doctor
from authApp.serializers.doctorSerializer import DoctorSerializer


class DoctorView(views.APIView):
    def get(self, request):
        medical = Doctor.objects.all()
        data = []
        for obj in medical:
            d = DoctorSerializer().to_representation(obj=obj)
            data.append(d)

        return Response(data, status=status.HTTP_201_CREATED)

    def post(self, request):
        serializer = DoctorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)
