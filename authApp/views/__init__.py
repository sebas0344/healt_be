from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .holaView import homepage
from .medicalAppointmentView import MedicalAppointmentView
from .doctorView import DoctorView
from .userView import UserView
