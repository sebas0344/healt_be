from rest_framework import status, views
from rest_framework.response import Response
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer


class UserView(views.APIView):
    def get(self, request):
        users = User.objects.all()  # Se buscan todos los usuarios en la base de datos
        data = []  # lista vacia
        for obj in users:  # for each para iterar entre los usuarios
            user = UserSerializer().to_representation(obj)  # Serializacion de obj a json
            # Se agrega a la lista el usuario en formato json
            data.append(user)

        # En el response se envia una lista de usuarios en formato json
        return Response(data=data, status=status.HTTP_201_CREATED)
