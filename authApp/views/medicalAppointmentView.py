import json
from authApp.models.medicalAppointment import MedicalAppointment
from authApp.serializers.appointmentSerializer import appointmentSerializer

from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

from authApp.models.doctor import Doctor
from authApp.serializers.doctorSerializer import DoctorSerializer

from rest_framework import status, views
from rest_framework.response import Response




class MedicalAppointmentView(views.APIView):

    # Crear método put que actualice la cita. Responsable: Juan
    def put(self, request):
        # aqui va el codigo
        # Se busca la cita con el mismo id del request
        cita = MedicalAppointment.objects.get(id=request.data["id"])
        serializer = appointmentSerializer(
            cita, data=request.data)  # Se actualizan los datos
        serializer.is_valid(raise_exception=True)  # Se validan
        serializer.save()  # Se guarda
        return Response(status=status.HTTP_201_CREATED)

    # Crear método delete que elimine una cita. Responsable: Camila

    def delete(self, request):
        # aqui va el codigo
        medical = MedicalAppointment.objects.get(id=request.data['id'])
        medical.delete()
        return Response(status=status.HTTP_201_CREATED)

    def post(self, request):
        # print(request.data)
        serializer = appointmentSerializer(
            data=request.data)  # se descerializa el request
        serializer.is_valid(raise_exception=True)  # validación
        serializer.save()  # Se almacena la cita en la base de datos

        return Response(status=status.HTTP_201_CREATED)

    
    def get(self, request,pk, format=None):

        usuario = MedicalAppointment.objects.filter(userId=pk)
        serializer = appointmentSerializer(usuario, many=True)

        data = []
        #contador = 0
        for u in serializer.data:
            #Serializar usuario
            usuarioAux = User.objects.get(pk=u['userId'])
            usuarioAuxSerializer = UserSerializer(usuarioAux,many=False)
            #print(usuarioAuxSerializer.data)
            
            #Serializar medico
            #doctor = Doctor.objects.get(pk=u['docId'])
            #doctorSerializer = DoctorSerializer(doctor, many=False)
            #print(doctorSerializer.data)
            #print(u['docId'])
            #data.append({'id':u['id'],'usuario':usuarioAuxSerializer.data['name'],'date':u['date'],'doctor':doctorSerializer.data['id']})
            data.append({'id':u['id'],'usuario':usuarioAuxSerializer.data['name'],'date':u['date']})
            #contador = contador + 1
        #print(serializer.data[0])
        print(data)
        #return Response(serializer.data)
        return Response(data)
    
    """""
    def get(self, request, format=None):
        snippets = MedicalAppointment.objects.all()
        serializer = appointmentSerializer(snippets, many=True)
        return Response(serializer.data)
     """
